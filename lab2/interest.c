#include <stdio.h>

int main(void) {
	int s=1000; /* sum borrowed, in GDP */
	double debt=s; /* actual debt */
	double rate=0.03; /* 3% interest rate per month */
	int month;
	double interest; /* declare other variables, month and interest */
	double total_interest; /* cumulative interest for each month */
	for (month=1; month<25; month++) {
		interest = debt * rate;
		debt += interest;
		total_interest = debt - s;
		printf("month %2d: debt=%7.2f, interest=%5.2f, total_interest=%7.2f, "\
				"frac=%6.2f%%\n",month,debt,interest,total_interest,total_interest/10);
	}
	return 0;
}

