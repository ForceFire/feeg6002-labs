#include <stdio.h>
#include <math.h>

#define XMIN 1.0	/*Starting x value*/
#define XMAX 10.0	/*Ending x value*/
#define N 10		/*Total number of points*/

int main(void) {
	int i=XMIN;
	double x;
	double y;
	double z;
	for (i=0; i<N; i++) {
		x = XMIN + ((XMAX - XMIN) / (N - 1.) * i);
		y = sin(x);
		z = cos(x);
		printf("%f %f %f\n",x,y,z);
	}
	
	return 0;
}

